This repo contains my work for upgrading ensembl versions for stemformatics.

It's in early days right now, but a few things are working.

Right now I'm not working with version info for ensembl ids.

## Scripts

### `retrieve_changed.py`

Retrieves all updates to ensembl ids from the enseml sql server. This essentially retrieves all the data used by the ensembl [id history converter](http://asia.ensembl.org/Homo_sapiens/Tools/IDMapper?db=core).

### `retrieve_gtf.py`

Retrieves gtf files for all releases it can find for a species from the ensembl ftp site, then extracts all unique gene and transcript ids it can find from them. Versions are not included, since they were not always available in gtf files. Parsed ids are sorted and placed into an hdf5 file.

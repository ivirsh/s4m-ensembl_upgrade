# This script retrieves all gtf files from ensembl and then creates a hdf file with all ids.

# TODO
# * Probably split out writing h5 file and retrieving gtfs.
# * Move most functions to libraries.
# * Add cli
# * Add support for more species ( right now will only work for humans I believe. )

from ftplib import FTP, error_perm
from pathlib import Path
from collections import namedtuple
import re
import pandas as pd
import numpy as np
import gzip
import h5py


BASE = re.compile(r"\d\.gtf\.gz")
ReleaseGandT = namedtuple("ReleaseGandT", ["gene", "transcript"])
gene_id_re = re.compile("(ENSG\d+)")
transcript_id_re = re.compile('"(ENST\d+)"')

## Getting genome files, probably not neccesary.
def md5_ftp_file(ftp, filepath):
    s = hashlib.md5()
    ftp.retrbinary("RETR {}".format(filepath), s.update)
    return s

def identify_genome(ftp, release, species="homo_sapiens"):
    """Returns sum of top level genome for a species."""
    dna_dir = Path("release-{}/fasta/{}/dna/".format(release, species))
    dna_files = ensembl.nlst(str(dna_dir))
    dna_files = filter(lambda x: "primary_assembly" in x, dna_files)
    dna_file = list(filter(lambda x: "dna_" not in x, dna_files))
    print(dna_file)
    assert len(dna_file) == 1, "Should only be one top level file."
    dna_file = dna_file[0]
    return md5_ftp_file(ensembl, dna_file)
#####
# General dealing with ensembl via ftp
####

def find_release_dirs(ftp):
    """
    Find all release dirs from ensembl ftp.

    Assumes ftp is already at base directory.
    """
    all_dirs = ftp.mlsd()
    dir_names = map(lambda x: x[0], all_dirs)
    release_dirs = filter(lambda x: re.match(r"release.*", x), dir_names)
    return sorted(release_dirs)

def retrieve_gtf(ftp, base_dir, local_pth, species="homo_sapiens"):
    """Retrieves gtf file from ensembl ftp site."""
    local_pth = Path(local_pth)
    gtf_dir = "{}/gtf/{}/".format(base_dir, species)
    dir_contents = ftp.mlsd(gtf_dir)
    gtf_file = list(filter(BASE.search, map(lambda x: x[0], dir_contents)))
    if len(gtf_file) != 1:
        raise OSError("{} files found in {}".format(len(gtf_file, gtf_dir)))
    gtf_file = gtf_file[0]
    gtf_pth = gtf_dir + gtf_file
    if local_pth.is_dir():
        local_pth = local_pth.joinpath(gtf_file)
    with local_pth.open("wb") as f:
        ftp.retrbinary("RETR {}".format(gtf_pth), f.write)

#####
# Parsing
#####

def extract_ensembl_gene_ids(pth):
    """Given path, returns dataframe of ensembl ids"""
    gene_ids = set()
    with gzip.open(str(pth), "rb") as f:
        for line in f:
            for found in gene_id_complete.findall(line.decode("utf-8")):
                gene_ids.add(found)
    return np.array(sorted(gene_ids), dtype=StableID)

def extract_ensembl_gene_transcipt_ids(pth):
    """
    Given path to compressed gtf, returns ensembl ids for genes and transcripts
    in separate np.arrays
    """
    gene_ids = set()
    transcript_ids = set()
    with gzip.open(str(pth), "rb") as f:
        for line in f:
            l_decoded = line.decode("utf-8")
            for found in gene_id_re.findall(l_decoded):
                gene_ids.add(found)
            for found in transcript_id_re.findall(l_decoded):
                transcript_ids.add(found)
    return ReleaseGandT(gene =       np.array(sorted(gene_ids), dtype="S15"),
                        transcript = np.array(sorted(transcript_ids), dtype="S15"))


#
# IMPORTANT LESSON:
#
# gtf files do not always have versions for their ids
#
# I'm not sure how this will effect being able to check versions in the future. Is there enough to infer from the changes? Could I get it elsewhere?
#
# Are gtf3 files available retroactivley?
def test_ensembl_ids(reference, test_set):
    assert len(reference) == len(test_set)
    assert (reference == test_set).all()


if __name__ == "__main__":
    # from argparse import ArgumentParser
    # parser = ArgumentParser(description="Retrieves and parses gtf files from ensembl.")
    # parser.add_argument("-d", "--datadir", type=Path, default=Path("./data"))
    # parser.add_argument("-g", "--gtfdir", type=Path, default=)# huh
    # parser.add_argument("-s", "--species", type=str, choices=["homo_sapiens", "mus_musc", ...])
    # Init
    datapth = Path("./data")
    gtfpth = datapth.joinpath("gtf")
    gtfpth.mkdir(parents=True, exist_ok=True)

    ensembl = FTP("ftp.ensembl.org")
    ensembl.login()
    ensembl.cwd("pub")

    # Finding releases
    release_dirs = find_release_dirs(ensembl)
    # Downloading gtf files
    for d in release_dirs:
        print(d)
        try: # Could probably handle this better.
            retrieve_gtf(ensembl, d, gtfpth)
        except Exception as E:
            print(E)
            pass
    ensembl.quit()

    # Reading in gtf files
    h5 = h5py.File("data/gene_ids.hdf5", "w")
    gtfs = gtfpth.glob("*gtf.gz")
    for gtf in gtfs:
        release = gtf.name.split(".")[-3]
        species = gtf.name.split(".")[0].lower()
        print("extracting from {}".format(gtf))
        data = extract_ensembl_gene_transcipt_ids(gtf)
        print("Found {} transcripts.".format(len(data.transcript)))
        print("Found {} genes.".format(len(data.gene)))
        h5.create_dataset("release-{}/{}/gene_ids".format(release, species), data=data.gene)
        h5.create_dataset("release-{}/{}/transcript_ids".format(release, species), data=data.transcript)
        h5.flush()
    h5.close()

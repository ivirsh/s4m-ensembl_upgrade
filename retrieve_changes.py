import pymysql.cursors
import pandas as pd
from argparse import ArgumentParser

def find_newest_db(c, name):
    """Find most recent release of database."""
    db_base = "{}_core_".format(name)
    with connection.cursor() as cursor:
        cursor.execute("show databases;")
        db_names = [x[0] for x in cursor.fetchall() if db_base in x[0]]
        assert len(db_names) != 0, "Couldn't find database for species {}.".format(args.species)
        db_name = sorted(db_names)[-1] # Newest version
    return db_name

if __name__ == "__main__":
    parser = ArgumentParser(description="Retrieves updates to ensembl ids.")
    parser.add_argument("-s", "--species", default="homo_sapiens", help="Species name (e.g. 'homo_sapiens')")

    args = parser.parse_args()

    fields = [  "old_stable_id",
            "old_version",
            "old_assembly",
            "old_release",
            "new_stable_id",
            "new_version",
            "new_assembly",
            "new_release",
            "score",
            "type"]
    connection = pymysql.connect(host="ensembldb.ensembl.org",
                             user="anonymous",
                             port=3306,
                             password="")#,
                            #  db="homo_sapiens_core_86_38")
    # Find db
    db_name = find_newest_db(connection, args.species)
    connection.select_db(db_name)
    print("Retrieving from database: {}".format(db_name))
    # Query db
    query = """
            SELECT  old_stable_id,
                    old_version,
                    old_assembly,
                    old_release,
                    new_stable_id,
                    new_version,
                    new_assembly,
                    new_release,
                    score,
                    type
            FROM    stable_id_event
              JOIN  mapping_session USING (mapping_session_id)
            ORDER BY old_version ASC, CAST(new_release AS UNSIGNED)
            """
    with connection.cursor() as cursor:
        cursor.execute(query)
        results = cursor.fetchall()

    df = pd.DataFrame(data=list(results), columns=fields)
    connection.close()
    df.to_csv("data/ensembl.tsv", sep="\t", index=False)

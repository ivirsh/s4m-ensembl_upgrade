"""This module contains my code for mapping between versions of ensembl"""

import pandas as pd
import numpy as np
import h5py

#####
# Working with ensembl history
#####

def read_updates_table(pth):
    """Read table of ensembl id changes in friendly manner."""
    return pd.read_table(pth,
        dtype={ "old_stable_id": np.dtype("S15"),
                "old_version": int,
                "old_assembly": np.dtype("S"),
                "old_release": float,
                "new_stable_id": np.dtype("S15"),
                "new_version": int,
                "new_assembly": np.dtype("S"),
                "new_release": float,
                "score": float,
                "type": np.dtype("S")})

def get_one_step_mappings(data, ensembl_ids):
    mappings = data[data["old_stable_id"].isin(ensembl_ids)]
    return dict([(old_id, subdf["new_stable_id"].unique()) for old_id, subdf in mappings.groupby("old_stable_id")])

# TODO: add type?
def filter_releases(data, old_version, new_version):
    return data[(data["old_release"] >= old_version) & (data["new_release"] <= new_version)]

# Do we care about identifiers which were created (from scratch) in between versions?
def get_all_updates(data, old_version, new_version):
    data = filter_releases(data, old_version, new_version)
#     to_check = data[["old_stable_id", "new_stable_id"]].stack().unique() # Right now, including this would have no effect
    to_check = data["old_stable_id"].unique()
    changes = get_one_step_mappings(data, to_check)
    return changes

def find_children(updates, ensembl_id, null_value=b""):
    to_check = list(updates[ensembl_id])
    maps_to = set()
    while to_check: # While not empty
        v = to_check.pop()
        if not (v is null_value): # Figure out how I want to deal with null string
            if v in updates:
                to_check.extend(updates[v])
        maps_to.add(v)
    return np.array(sorted(maps_to), dtype=np.dtype("S15")) # may need to change too

#####
# Working with id lists
#####

def retrieve_ids(hdf, release, species="homo_sapiens", kind="gene"):
    """Retrieve ensembl ids from hdf5 file"""
    dataset = "release-{}/{}/{}_ids".format(release, species, kind)
    return hdf.get(dataset).value

def get_final_mappings(updates, old_ids, new_ids):
    """
    Given edges of update graph, find what nodes in `old_ids` end up mapping to in `new_ids`.
    """
    changed = old_ids[np.in1d(old_ids, list(updates.keys()))]
    connected_components = np.array([find_children(updates, x) for x in changed])
    connected_ultimate = connected_components.copy() # Just allocating
    all_connected = np.unique(np.concatenate(connected_components))
    all_connected.sort()
    ultimate_connected = all_connected[np.in1d(all_connected, new_ids, assume_unique=True)]
    for i, vals in enumerate(connected_components):
        connected_ultimate[i] = vals[np.in1d(vals, ultimate_connected, assume_unique=True)]
    return changed, connected_ultimate
